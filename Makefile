build-all: 
	docker-compose build

build-app: 
	docker-compose build app

build-api: 
	docker-compose build api

start-app:
	docker-compose up app 

start-api:
	docker-compose up api 

test-app:
	docker-compose exec app npm run test $(ARGS)

test-api:
	docker-compose exec api npm run test $(ARGS) -- --runInBand

test:
	docker-compose exec app npm run test
	docker-compose exec api npm run test -- --runInBand

before-commit:
	docker-compose exec app npm run test
	docker-compose exec api npm run test -- --runInBand
	docker-compose exec app npm run e2e

down:
	docker-compose down

e2e:
	docker-compose run --rm app npm run e2e $(ARGS)
