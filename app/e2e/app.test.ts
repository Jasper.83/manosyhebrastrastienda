import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core'
import {render, screen} from '@testing-library/angular'
import { AppComponent } from '../src/app/app.component'
import { HttpClientModule } from '@angular/common/http'
import userEvent from '@testing-library/user-event'
import { FormsModule } from '@angular/forms'

describe('App', () => {
    beforeEach( async() => {
        await render(AppComponent, {
            detectChanges: true,
            imports: [HttpClientModule, FormsModule],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        })
    })

    it('shows confirmation message when adds a product', async () => {
        let confirmationMessage = 'Producto añadido correctamente'
        const button = screen.getByRole('button')
        await userEvent.click(button)

        let message = await screen.findByText(confirmationMessage)

        expect(message).toBeInTheDocument()
    })
    it('Show formulary clean, when adds a product', async() => {
        const aProduct = 'a product'
        const products = await screen.getByLabelText('Nombre del producto')
        const categories = await screen.getByRole('combobox') 
        const category = await screen.getByRole('option', { name: 'Ovillos' }) 
        const price = await screen.getByLabelText('Precio del producto')
        const button = await screen.getByRole('button')
        await userEvent.type(products, aProduct)
        await userEvent.selectOptions(categories, category) 
        await userEvent.type(price, '3.50')
        
        await userEvent.click(button)
        
        expect((products as HTMLInputElement).value).toBe('')
        expect((category as HTMLOptionElement).selected).toBe(false)
        expect((price as HTMLInputElement).value).toBe('')

    })
})
