import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Manos y Hebras, trastienda';
  product: string = '';
  category: string = '';
  price: string = '';
  image: any
  textOfMessage: string = ''
  

  constructor(private service: AppService) {}

  ngOnInit() {
 
  }

  onClick(): void {
    this.textOfMessage = 'Producto añadido correctamente'
    this.cleanInput()
  }

  cleanInput(): void{
      this.product = ''
      this.category = ''
      this.price = ''
    
  }
  getNewProduct(event:string): void {
    this.service.saveProduct(this.product, event).subscribe(() => {
    })
  }
}
